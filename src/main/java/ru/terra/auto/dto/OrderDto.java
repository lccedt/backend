package ru.terra.auto.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class OrderDto extends AbstractDto {

    @Size(min = 4, max = 20)
    private String startPoint;

    @Size(min = 4, max = 20)
    private String endPoint;

    private String startPointLocation;

    private String endPointLocation;
}
