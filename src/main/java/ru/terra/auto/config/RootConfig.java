package ru.terra.auto.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


/**
 * @author Evgeniy Kobtsev
 */
@Configuration
@ComponentScan(basePackages={"ru.terra.auto"},
        excludeFilters={ @Filter(type=FilterType.ANNOTATION, value=EnableWebMvc.class) })
public class RootConfig {

    @Bean
    public ModelMapper modelMapper() {
                return new ModelMapper();
        }

}