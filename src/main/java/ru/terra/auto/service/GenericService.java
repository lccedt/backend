package ru.terra.auto.service;

import java.util.List;

import ru.terra.auto.dto.AbstractDto;

/**
 * Created by zhogin on 11/23/2016.
 */
public interface GenericService<D extends AbstractDto> {

    void save(D entity);

    void update(D entity);

    List<D> findAll();

    D findOne(Long id);

    boolean exists(Long id);

    long count();

    void delete(Long id);

    void delete(D entity);

    void deleteAll();

}
