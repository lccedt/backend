package ru.terra.auto.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.model.User;
import ru.terra.auto.model.UserRole;
import ru.terra.auto.model.enums.UserRoleType;
import ru.terra.auto.repository.UserRepository;
import ru.terra.auto.repository.UserRoleRepository;
import ru.terra.auto.service.UserService;

/**
 * Created by zhogin on 11/23/2016.
 */
@Service
@Slf4j
public class UserServiceImpl extends GenericServiceImpl<User, UserDto, UserRepository> implements UserService {

    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, ModelMapper modelMapper, UserRoleRepository userRoleRepository) {
        super(repository, modelMapper);
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserDto getByUsername(String username) {
        User user = repository.findByUsername(username);
        if (user == null) {
            return null;
        }
        return convertToDTO(user);
    }

    @Override
    public RegistrationUserDto getByUsernameWithPassword(String username) {
        User user = repository.findByUsername(username);
        if (user == null) {
            return null;
        }
        return convertToDTOWithPassword(user);
    }

    private RegistrationUserDto convertToDTOWithPassword(User user) {
        return modelMapper.map(user, RegistrationUserDto.class);
    }

    @Override
    public List<UserRole> getUserRolesById(Long id) {
        return userRoleRepository.findByUsersId(id);
    }

    @Override
    public void saveNewUser(RegistrationUserDto userDTO) {
        String encodedPassword = new BCryptPasswordEncoder().encode(userDTO.getPassword());
        userDTO.setPassword(encodedPassword);
        User user = convertToEntity(userDTO);
        List<UserRole> userRoles = new ArrayList<>();
        userRoles.add(userRoleRepository.findByRoleName(UserRoleType.USER.getRoleName()));
        user.setUserRoles(userRoles);
        repository.save(user);
    }

    @Override
    protected User convertToEntity(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    protected UserDto convertToDTO(User entity) {
        return modelMapper.map(entity, UserDto.class);
    }

    @Override
    protected void mapToExistingEntity(User entity, UserDto dto) {
        modelMapper.map(dto, entity);
    }
}
