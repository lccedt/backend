package ru.terra.auto.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.terra.auto.dto.OrderDto;
import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.model.Order;
import ru.terra.auto.model.User;
import ru.terra.auto.model.UserRole;
import ru.terra.auto.model.enums.UserRoleType;
import ru.terra.auto.repository.OrderRepository;
import ru.terra.auto.repository.UserRepository;
import ru.terra.auto.repository.UserRoleRepository;
import ru.terra.auto.service.OrderService;
import ru.terra.auto.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Zhogin
 */
@Service
@Slf4j
public class OrderServiceImpl extends GenericServiceImpl<Order, OrderDto, OrderRepository> implements OrderService {

    @Autowired
    public OrderServiceImpl(OrderRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }


    @Override
    protected Order convertToEntity(OrderDto dto) {
        return modelMapper.map(dto, Order.class);
    }

    @Override
    protected OrderDto convertToDTO(Order entity) {
        return modelMapper.map(entity, OrderDto.class);
    }

    @Override
    protected void mapToExistingEntity(Order entity, OrderDto dto) {
        modelMapper.map(dto, entity);
    }
}
