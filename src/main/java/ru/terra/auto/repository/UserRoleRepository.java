package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;

import java.util.List;

import ru.terra.auto.model.UserRole;

/**
 * Created by zhogin on 12/13/2016.
 */
@Repository
public interface UserRoleRepository extends GenericRepository<UserRole> {
    UserRole findByRoleName(String roleName);

    List<UserRole> findByUsersId(Long userId);
}
