package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;
import ru.terra.auto.model.Order;
import ru.terra.auto.model.User;

/**
 * @author Denis Zhogin
 */
@Repository("orderRepository")
public interface OrderRepository extends GenericRepository<Order> {

}
