package ru.terra.auto.model.enums;

/**
 * @author Evgeniy Kobtsev
 */
public enum VehicleBodyType {

	ONEDOOR, TWODOOR

}
