package ru.terra.auto.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@Entity
@Table(name = "USERS")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class User extends AbstractEntity {

    @Column(name = "USERNAME", nullable = false, updatable = false, unique = true)
    private String username;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "MIDDLENAME")
    private String middlename;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "SKYPE")
    private String skypeName;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "CITY")
    private String city;

    @OneToMany(mappedBy = "user")
    private List<CargoRequest> cargoRequests;

    @OneToMany(mappedBy = "user")
    private List<VehicleRequest> vehicleRequests;

    @OneToMany(mappedBy = "user")
    private List<Vehicle> vehicles;

    @OneToMany(mappedBy = "user")
    private List<Order> orders;

    @ManyToMany
    @JoinTable(name = "APP_USERS_ROLES", joinColumns = {
            @JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")})
    private List<UserRole> userRoles;

}
