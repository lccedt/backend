package ru.terra.auto.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.model.embedded.VehicleSpecificParams;

import javax.persistence.*;

/**
 * Created by zhogin on 12/6/2016.
 */
@Entity
@Table(name = "ORDERS")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Order extends AbstractEntity {

    @OneToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    @Column(name = "START_POINT")
    private String startPoint;

    @Column(name = "END_POINT")
    private String endPoint;

    @Column(name = "START_POINT_LOCATION")
    private String startPointLocation;

    @Column(name = "END_POINT_LOCATION")
    private String endPointLocation;

}
