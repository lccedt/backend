package ru.terra.auto.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.LoadType;
import ru.terra.auto.model.embedded.RouteInfo;

/**
 * @author Evgeniy Kobtsev
 */
@Entity
@Table(name = "VEHICLE_REQUEST")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class VehicleRequest extends AbstractEntity {

	@Embedded
	private RouteInfo routeInfo;

    @ManyToMany(cascade = {CascadeType.REFRESH})
    @JoinTable(name = "VEHICLE_REQ_LOAD_TYPE_DICT", joinColumns = {
            @JoinColumn(name = "VEHICLE_REQ_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "LOAD_TYPE_ID",
                    nullable = false, updatable = false) })
    private List<LoadType> supportedLoadTypes;

	@ManyToOne
	@JoinColumn(name = "VEHICLE_ID", nullable = false)
	private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

	@Column(name = "PAYMENT_INFO")
	private String paymentInfo;

	@Column(name = "COMMENT")
	private String comment;

}
