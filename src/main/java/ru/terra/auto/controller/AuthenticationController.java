package ru.terra.auto.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.terra.auto.dto.ApiResponseDto;
import ru.terra.auto.dto.LoginResponseDto;
import ru.terra.auto.dto.LoginUserDto;
import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.exception.http.AppException;
import ru.terra.auto.model.User;
import ru.terra.auto.model.UserRole;
import ru.terra.auto.model.enums.UserRoleType;
import ru.terra.auto.repository.UserRepository;
import ru.terra.auto.repository.UserRoleRepository;
import ru.terra.auto.security.JwtTokenProvider;
import ru.terra.auto.service.UserService;

import java.net.URI;
import java.util.Collections;

/**
 * Created by zhogin on 11/28/2016.
 */
@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthenticationController {

    private AuthenticationManager authenticationManager;

    private PasswordEncoder passwordEncoder;

    private JwtTokenProvider tokenProvider;

    private UserService userService;

    private UserRepository userRepository;

    private UserRoleRepository userRoleRepository;

    @Autowired
    public AuthenticationController(UserService userService, UserRepository userRepository,
                                    UserRoleRepository userRoleRepository,
                                    AuthenticationManager authenticationManager,
                                    PasswordEncoder passwordEncoder,
                                    JwtTokenProvider tokenProvider) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegistrationUserDto registrationUserDto) {
        if (userRepository.existsByUsername(registrationUserDto.getUsername())) {
            return new ResponseEntity(new ApiResponseDto(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(registrationUserDto.getEmail())) {
            return new ResponseEntity(new ApiResponseDto(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        userService.saveNewUser(registrationUserDto);

//        URI location = ServletUriComponentsBuilder
//                .fromCurrentContextPath().path("/users/{username}")
//                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentContextPath().path("/").build().toUri()).body(new ApiResponseDto(true, "User registered successfully"));
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginUserDto loginUserDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUserDto.getUsername(),
                        loginUserDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);

        return ResponseEntity.ok(new LoginResponseDto(jwt, loginUserDto.getUsername()));
    }

}
